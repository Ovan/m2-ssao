#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec4 weight;
layout (location = 4) in ivec4 boneid;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
    mat4 model_view = view * model;
    
    gl_Position = projection * model_view * vec4(position, 1.0f);
    FragPos = vec3(model_view * vec4(position, 1.0));
    
    mat3 normalMatrix = transpose(inverse(mat3(model_view)));
    Normal = normalMatrix * normal;
    TexCoords = texCoords;
}