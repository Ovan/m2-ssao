#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec4 weight;
layout (location = 4) in ivec4 boneid;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform mat2x4 anim_bones[100];

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;

mat4 dual_quaternion_cast(vec4 real, vec4 dual)
{
    float magnitude = dot(real, real);
    float w = real.w, x = real.x, y = real.y, z = real.z;
    float t0 = dual.w, t1 = dual.x, t2 = dual.y, t3 = dual.z;
    
    mat4 m;
    m[0][0] = w * w + x * x - y * y - z * z;
    m[0][1] = 2 * x * y + 2 * w * z;
    m[0][2] = 2 * x * z - 2 * w * y;
    m[0][3] = 0;
    
    m[1][0] = 2 * x * y - 2 * w * z;
    m[1][1] = w * w + y * y - x * x - z * z;
    m[1][2] = 2 * y * z + 2 * w * x;
    m[1][3] = 0;
    
    m[2][0] = 2 * x * z + 2 * w * y;
    m[2][1] = 2 * y * z - 2 * w * x;
    m[2][2] = w * w + z * z - x * x - y * y;
    m[2][3] = 0;

    m[3][0] = -2 * t0 * x + 2 * w * t1 - 2 * t2 * z + 2 * y * t3;
    m[3][1] = -2 * t0 * y + 2 * t1 * z - 2 * x * t3 + 2 * w * t2;
    m[3][2] = -2 * t0 * z + 2 * x * t2 + 2 * w * t3 - 2 * t1 * y;
    m[3][3] = magnitude;

    return m / magnitude;
}

mat4 build_animation_matrix()
{
    mat2x4 dq0 = anim_bones[boneid.x];
    mat2x4 dq1 = anim_bones[boneid.y];
    mat2x4 dq2 = anim_bones[boneid.z];
    mat2x4 dq3 = anim_bones[boneid.w];

    if(dot(dq0[0], dq1[0]) < 0.0) dq1 *= -1.0;
    if(dot(dq0[0], dq2[0]) < 0.0) dq2 *= -1.0;
    if(dot(dq0[0], dq3[0]) < 0.0) dq3 *= -1.0;

    mat2x4 result =
        dq0 * weight.x +
        dq1 * weight.y +
        dq2 * weight.z +
        dq3 * weight.w;

    return dual_quaternion_cast(result[0], result[1]);
}

void main()
{
    mat4 model_view = view * model * build_animation_matrix();
    
    gl_Position = projection * model_view * vec4(position, 1.0f);
    FragPos = vec3(model_view * vec4(position, 1.0));
    
    mat3 normalMatrix = transpose(inverse(mat3(model_view)));
    Normal = normalMatrix * normal;
    TexCoords = texCoords;
}
