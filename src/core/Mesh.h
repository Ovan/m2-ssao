#pragma once

#include <vector>

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>

#include "Shader.h"

#define NUM_BONES 4

struct Vertex
{
    Vertex()
    {
        std::memset(IDs, 0, sizeof(IDs));
        std::memset(Weights, 0, sizeof(Weights));
    }
    
    Vertex(glm::vec3 _p, glm::vec3 _n, glm::vec2 _c)
        : Position(_p), Normal(_n), TexCoords(_c)
    {
        std::memset(IDs, 0, sizeof(IDs));
        std::memset(Weights, 0, sizeof(Weights));
    }
    
    void addBoneData(uint bone_id, float weight)
    {
        for(unsigned int i = 0; i < NUM_BONES; i++) 
        {
            if(Weights[i] == 0.0) 
            {
                IDs[i] = bone_id;
                Weights[i] = weight;
                return;
            }
        }
    }
    
    // mesh data
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    
    // animation data
    float Weights[NUM_BONES];
    unsigned char IDs[NUM_BONES];
};

struct Texture
{
    GLuint id;
    std::string type;
    aiString path;
};

class Mesh
{
    public:
        Mesh();
        
        Mesh(
            std::vector<Vertex> vertices,
            std::vector<GLuint> indices,
            std::vector<Texture> textures
        );
        
        // re-send data to the GPU
        void update();
        
        void draw(const Shader &shader);
    public:
        std::vector<Vertex> vertices;
        std::vector<GLuint> indices;
        std::vector<Texture> textures;
    private:
        GLuint VAO, VBO, EBO;
};
