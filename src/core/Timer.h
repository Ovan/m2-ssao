#ifndef TIMER
#define TIMER

#include <chrono>

class timer
{
    public:
        timer() noexcept;
        ~timer() noexcept;

        void pause(bool) noexcept;

        void update() noexcept;
        void reset() noexcept;

        double getTimeMin() const noexcept  { return getTimeSec()/60;      }
        double getTimeSec() const noexcept  { return getTimeMsec()/1000.f; }
        double getTimeMsec() const noexcept { return timestamp;            }
    private:
        std::chrono::steady_clock::time_point timestart;
        std::chrono::steady_clock::time_point time;
        double timestamp;
};

#endif
