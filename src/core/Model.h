#pragma once

#include <glm/glm.hpp>
#include <SOIL/SOIL.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vector>
#include <memory>
#include <map>

#include "Mesh.h"

Texture TextureFromFile(std::string filename, std::string typeName = "");

class Model
{
    public:
        Model();
        Model(std::string path);
        
        std::vector<std::shared_ptr<Mesh>> meshes;
        glm::mat4 model;
        
        virtual void draw(const Shader &shader, float time=0.f);
    protected:
        // loading data position, normal, bones, ...
        virtual void processNode(aiNode* node);
        std::shared_ptr<Mesh> processMesh(aiMesh* mesh);
        //
        std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
    protected:
        Assimp::Importer importer;
        const aiScene* scene;
        //
        std::string directory;
        std::vector<Texture> textures_loaded;
};
