#pragma once

#include "Camera.h"
#include <GLFW/glfw3.h>

class Window
{
    public:
        Window(int width, int height);
        ~Window();
        
        void hideCursor();
        
        void onKeyEvent();
        void onMouseEvent();
        void onScrollEvent();
        
        Camera& getCamera() { return camera; }
        
        int getWidth()  const { return width; }
        int getHeight() const { return height; }
        float getAspect() const { return (float)width / (float)height; }
        
        GLuint draw_mode;
        GLuint wireframe;
        GLuint pause;
        
        bool run();
        void swap();
    protected:
        bool keys[1024];
        bool keysPressed[1024];
        GLfloat lastX , lastY;
        bool firstMouse;
        GLfloat deltaTime;
        GLfloat lastFrame;
    private:
        int width, height;
        GLFWwindow *window;
        Camera camera;
        
    private:
        friend void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
        friend void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
        friend void mouse_callback(GLFWwindow* window, double xpos, double ypos);
};
