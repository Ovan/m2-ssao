#include "Window.h"
#include <cstring>

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

Window::Window(int w, int h)
    : camera(glm::vec3(0.0f, 0.0f, 5.0f)),
      width(w), height(h), wireframe(false), pause(false),
      lastX(400), lastY(300), firstMouse(true),
      deltaTime(0.0f), lastFrame(0.0f), draw_mode(1)
{
    window = glfwCreateWindow(width, height, "TP Rendue OpenGL", nullptr, nullptr);
    glfwSetWindowUserPointer(window, reinterpret_cast<void*>(this));
    glfwMakeContextCurrent(window);
    
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    
    glewExperimental = GL_TRUE;
    glewInit();

    glViewport(0, 0, width, height);
    glEnable(GL_DEPTH_TEST);
    
    std::memset(keys, 0, sizeof(keys));
    std::memset(keysPressed, 0, sizeof(keysPressed));
}

Window::~Window()
{
}

void Window::hideCursor()
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Window::onKeyEvent()
{
    if(keys[GLFW_KEY_Z]) camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_Q]) camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_S]) camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_D]) camera.ProcessKeyboard(RIGHT, deltaTime);
  
    if(keys[GLFW_KEY_0]) draw_mode = 0;
    if(keys[GLFW_KEY_1]) draw_mode = 1;
    if(keys[GLFW_KEY_2]) draw_mode = 2;
    if(keys[GLFW_KEY_3]) draw_mode = 3;
    if(keys[GLFW_KEY_4]) draw_mode = 4;
    if(keys[GLFW_KEY_5]) draw_mode = 5;
    if(keys[GLFW_KEY_6]) draw_mode = 6;
    if(keys[GLFW_KEY_7]) draw_mode = 7;
    if(keys[GLFW_KEY_8]) draw_mode = 8;
    if(keys[GLFW_KEY_9]) draw_mode = 9;
    
    if(keys[GLFW_KEY_P]) wireframe = !wireframe;
    if(keys[GLFW_KEY_T]) pause = !pause;
}

void Window::onMouseEvent()
{
}

void Window::onScrollEvent()
{
}

bool Window::run()
{
    glfwPollEvents();
    onKeyEvent();
    return !glfwWindowShouldClose(window);
}

void Window::swap()
{
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    // Swap the buffers
    glfwSwapBuffers(window);
}

/////////////////////////////////////////////////////////////////////////////////

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    Window *wind = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(key >= 0 && key <= 1024)
    {
        if(action == GLFW_PRESS)
            wind->keys[key] = true;
        else if(action == GLFW_RELEASE)
        {
            wind->keys[key] = false;
            wind->keysPressed[key] = false;
        }
    }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    Window *wind = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    
    if(wind->firstMouse)
    {
        wind->lastX = xpos;
        wind->lastY = ypos;
        wind->firstMouse = false;
    }

    GLfloat xoffset = xpos - wind->lastX;
    GLfloat yoffset = wind->lastY - ypos;

    wind->lastX = xpos;
    wind->lastY = ypos;

    wind->getCamera().ProcessMouseMovement(xoffset, yoffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    //Window *wind = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    //wind->getCamera().ProcessMouseScroll(yoffset);
}