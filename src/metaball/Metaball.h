#ifndef __SAMPLE_META_BALL__
#define __SAMPLE_META_BALL__

#include "../core/Mesh.h"
#include <glm/glm.hpp>

struct MetaBallPosition
{
    glm::vec3 position;
    float squaredRadius;
};

struct VoxelVertex
{
    glm::vec3 position;
    glm::vec3 normal;
    float value;
};

struct VoxelCube
{
    VoxelVertex * vertices[8];
};

class Metaball : public Mesh
{
    public:
        Metaball(int size = 40);
        ~Metaball();

        void setThreshold(float);
        float getThreshold() const noexcept;

        std::vector<MetaBallPosition> balls;

        void move();

        virtual void update();
    protected:
        std::vector<VoxelVertex> grid;
        std::vector<VoxelCube> cubes;
    protected:
        float threshold;
};

#endif
