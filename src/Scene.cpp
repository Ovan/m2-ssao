#include "Scene.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

Scene::Scene(Window *window)
    : wnd(window),
      lbs("../resources/objects/cylinder/anim_cylinder.fbx"),
      dqs("../resources/objects/cylinder/anim_cylinder.fbx"),
      mdl("../resources/objects/spider/spider.fbx")
{
    woodTexture = TextureFromFile("../resources/textures/bricks2.jpg");
    GLfloat vertices[] = {
        // Back face
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
        0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
        0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
        0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
        -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
        // Front face
        -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
        0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
        0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
        0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
        -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
        -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
        // Left face
        -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
        -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
        -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
        -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
        -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
        -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
        // Right face
        0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
        0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
        0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right
        0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
        0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
        0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left
        // Bottom face
        -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
        0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
        0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
        0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
        -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
        -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
        // Top face
        -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
        0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
        0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right
        0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
        -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
        -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
        -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
    };
    
    glGenVertexArrays(1, &cubeVAO);
    glGenBuffers(1, &cubeVBO);
    glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    glBindVertexArray(cubeVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    for(int i=0; i<3; i++)
        metaball.balls.push_back({glm::vec3(0.0f, 0.0f, 0.0f), 25.0f*(i+1)});
  
    metaball.move();
    metaball.update();
    metaball.textures.push_back(TextureFromFile("../resources/textures/metal.png", "texture_diffuse"));
    
    metaball.setThreshold(2.0f);
}

Scene::~Scene()
{
}

void Scene::render_room(Shader &shader)
{
    shader.bind();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, woodTexture.id);
    glBindVertexArray(cubeVAO);
    
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(10.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(1.0f, 20.0f, 20.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);

    model = glm::mat4();
    model = glm::translate(model, glm::vec3(-10.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(1.0f, 20.0f, 20.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);

    model = glm::mat4();
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 10.0f));
    model = glm::scale(model, glm::vec3(20.0f, 20.0f, 1.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);

    model = glm::mat4();
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -10.0f));
    model = glm::scale(model, glm::vec3(20.0f, 20.0f, 1.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    
    model = glm::mat4();
    model = glm::translate(model, glm::vec3(0.0f, 10.0f, 0.0f));
    model = glm::scale(model, glm::vec3(20.0f, 1.0f, 20.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);

    // Floor cube
    model = glm::mat4();
    model = glm::translate(model, glm::vec3(0.0, -1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(20.0f, 1.0f, 20.0f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    
    glBindVertexArray(0);
}

void Scene::render_metaball(Shader &shader)
{
    float c = 2.0f*cos(tm.getTimeSec());
  
    metaball.balls[0].position.x = -8.0f*cos(tm.getTimeSec()*1.7) - c;
    metaball.balls[0].position.y =  8.0f*sin(tm.getTimeSec()*1.6) - c;
    metaball.balls[1].position.y =  10.0f*sin(tm.getTimeSec()*1.4) + c;
    metaball.balls[1].position.z =  10.0f*cos(tm.getTimeSec()*1.4) - c;
    metaball.balls[2].position.x = -15.0f*cos(tm.getTimeSec()*1.4) - 0.2f*sin(tm.getTimeSec()*1.6);
    metaball.balls[2].position.z =  15.0f*sin(tm.getTimeSec()*1.5) - 0.2f*sin(tm.getTimeSec()*1.4);
  
    metaball.move();
    metaball.update();
    
    glm::mat4 model = glm::mat4();
    model = glm::translate(model, glm::vec3(0.0f, 2.0f, 0.0));
    model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0, 0.0, 0.0));
    model = glm::scale(model, glm::vec3(0.1f));
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    metaball.draw(shader);
}

void Scene::render(GBuffer *gbuffer)
{
    if(!wnd->pause)
        tm.update();
    
    //--------------------------------------------------
      
    Shader &screen = gbuffer->getStaticObjectShader();
    Shader &linear = gbuffer->getLinearBlendShader();
    Shader &quater = gbuffer->getDualQuatBlendShader();
    
    //--------------------------------------------------
    
    render_room(screen);
    render_metaball(screen);
    
    //--------------------------------------------------
    
    lbs.model = glm::mat4();
    lbs.model = glm::translate(lbs.model, glm::vec3(2.0f, 2.0f, 5.0));
    lbs.model = glm::scale(lbs.model, glm::vec3(0.005f));
    lbs.draw(linear, tm.getTimeSec());
    
    //--------------------------------------------------
    
    dqs.model = glm::mat4();
    dqs.model = glm::translate(dqs.model, glm::vec3(-2.0f, 2.0f, 5.0));
    dqs.model = glm::scale(dqs.model, glm::vec3(0.5f));
    dqs.draw(quater, tm.getTimeSec());
    
    //--------------------------------------------------
    
    /*
    mdl.model = glm::mat4();
    mdl.model = glm::translate(mdl.model, glm::vec3(0.0f, 2.0f, 0.0));
    mdl.model = glm::scale(mdl.model, glm::vec3(0.05f));
    mdl.draw(linear, tm.getTimeSec());
    */
}
