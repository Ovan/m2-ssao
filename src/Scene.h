#pragma once

#include "core/Window.h"
#include "core/Timer.h"
#include "effect/GBuffer.h"
#include "metaball/Metaball.h"
#include "skinning/AnimationLBS.h"
#include "skinning/AnimationDQS.h"

class Scene
{
    public:
        Scene(Window*);
        ~Scene();
        
        void render_room(Shader&);
        void render_metaball(Shader&);

        void render(GBuffer*);
    protected:
        Window *wnd;
        Metaball metaball;
        timer tm;
    private:
        AnimationLBS lbs;
        AnimationDQS dqs;
        AnimationLBS mdl;
        Texture woodTexture;
        GLuint cubeVAO;
        GLuint cubeVBO;
};
