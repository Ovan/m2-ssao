#pragma once

#include "GBuffer.h"
#include "../core/Shader.h"

#include <vector>

class Lighting
{
    public:
        Lighting(GBuffer *gbuffer);
        ~Lighting();
        
        void render(
            glm::mat4 &view, glm::mat4 &projection,
            GLuint albedo, GLuint ssao, int mode
        );
    private:
        int width, height;
        GBuffer *gbuffer;
        Shader lightShader;
};
