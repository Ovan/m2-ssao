#include "FXAA.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

FXAA::FXAA(GBuffer *g)
    : gbuffer(g), width(g->getWidth()), height(g->getHeight()),
      fxaaShader("../resources/shaders/utils/quadVertex.glsl", "../resources/shaders/fxaa/fxaaFragment.glsl")
{
    glGenFramebuffers(1, &fxaaFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, fxaaFBO);
    
    glGenTextures(1, &fxaaColorBuffer);
    glBindTexture(GL_TEXTURE_2D, fxaaColorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fxaaColorBuffer, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    fxaaShader.bind();
    glUniform1i(glGetUniformLocation(fxaaShader.Program, "gPosition"), 0);
    glUniform1i(glGetUniformLocation(fxaaShader.Program, "gNormal"), 1);
    glUniform1i(glGetUniformLocation(fxaaShader.Program, "gAlbedo"), 2);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FXAA::~FXAA()
{
}

void FXAA::render(glm::mat4 &view, glm::mat4 &projection)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fxaaFBO);
    glClear(GL_COLOR_BUFFER_BIT);
    fxaaShader.bind();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gbuffer->getFBPosition());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gbuffer->getFBNormal());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gbuffer->getFBAlbedo());
    
    gbuffer->render_quad();
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
