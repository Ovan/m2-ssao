#include "Lighting.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <random>


GLfloat lerp(GLfloat a, GLfloat b, GLfloat f);

Lighting::Lighting(GBuffer *g)
    : gbuffer(g), width(g->getWidth()), height(g->getHeight()),
      lightShader("../resources/shaders/utils/quadVertex.glsl", "../resources/shaders/lighting/lightFragment.glsl")
{
    lightShader.bind();
    glUniform1i(glGetUniformLocation(lightShader.Program, "gPosition"), 0);
    glUniform1i(glGetUniformLocation(lightShader.Program, "gNormal"), 1);
    glUniform1i(glGetUniformLocation(lightShader.Program, "gAlbedo"), 2);
    glUniform1i(glGetUniformLocation(lightShader.Program, "ssao"), 3);
}

Lighting::~Lighting()
{
}

void Lighting::render(glm::mat4 &view, glm::mat4 &projection, GLuint albedo, GLuint ssao, int mode)
{
    glm::vec3 lightPosition = glm::vec3(2.0, 4.0, -2.0);
    glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
    glm::vec3 lightPosView = glm::vec3(view * glm::vec4(lightPosition, 1.0));
    
    const GLfloat constant = 1.0;
    const GLfloat linear = 0.12;
    const GLfloat quadratic = 0.023;
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    lightShader.bind();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gbuffer->getFBPosition());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gbuffer->getFBNormal());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, albedo);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, ssao);
    
    glUniform3fv(glGetUniformLocation(lightShader.Program, "light.Position"), 1, &lightPosView[0]);
    glUniform3fv(glGetUniformLocation(lightShader.Program, "light.Color"), 1, &lightColor[0]);
    glUniform1f(glGetUniformLocation(lightShader.Program, "light.Constant"), constant);
    glUniform1f(glGetUniformLocation(lightShader.Program, "light.Linear"), linear);
    glUniform1f(glGetUniformLocation(lightShader.Program, "light.Quadratic"), quadratic);
    glUniform1i(glGetUniformLocation(lightShader.Program, "draw_mode"), mode);
    
    gbuffer->render_quad();
}
