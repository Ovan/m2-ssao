#pragma once

#include "GBuffer.h"
#include "../core/Shader.h"

#include <vector>

class FXAA
{
    public:
        FXAA(GBuffer *gbuffer);
        ~FXAA();
        
        GLuint getFrameBuffer() const { return fxaaFBO; }
        
        GLuint getFBFXAA() const { return fxaaColorBuffer; }
        
        void render(glm::mat4 &view, glm::mat4 &projection);
    private:
        int width, height;
        GBuffer *gbuffer;
        // pass dependent buffer
        GLuint fxaaFBO;
        GLuint fxaaColorBuffer;
        Shader fxaaShader;
};
