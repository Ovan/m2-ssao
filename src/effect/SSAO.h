#pragma once

#include "GBuffer.h"
#include "../core/Shader.h"

#include <vector>

class SSAO
{
    public:
        SSAO(GBuffer *gbuffer);
        ~SSAO();
        
        GLuint getFrameBuffer0()   const { return ssaoFBO; }
        GLuint getFrameBuffer1()   const { return ssaoBlurFBO; }
        
        GLuint getFBSSAO()   const { return ssaoColorBuffer; }
        GLuint getFBBlured() const { return ssaoColorBufferBlur; }
        
        void render(glm::mat4 &view, glm::mat4 &projection);
    protected:
        void pass_ssao(glm::mat4 &view, glm::mat4 &projection);
        void pass_blur();
        void setup_random_texture();
    private:
        int width, height;
        GBuffer *gbuffer;
        // pass dependent buffer
        GLuint ssaoFBO,         ssaoBlurFBO;
        GLuint ssaoColorBuffer, ssaoColorBufferBlur;
        Shader ssaoShader,      blurShader;
        // noise infos
        std::vector<glm::vec3> ssaoKernel;
        GLuint noiseTexture;
};
