#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "../core/Shader.h"

class GBuffer
{
    public:
        GBuffer(int width, int height);
        ~GBuffer();

        void bind(glm::mat4 view, glm::mat4 projection);
        void render_quad();
        void unbind();
        
        Shader& getStaticObjectShader()  { return screenShader; }
        Shader& getLinearBlendShader()   { return linearShader; }
        Shader& getDualQuatBlendShader() { return quaterShader; }
        
        GLuint getFBPosition()  const { return gPosition; }
        GLuint getFBNormal()    const { return gNormal; }
        GLuint getFBAlbedo()    const { return gAlbedo; }
        
        GLuint getFrameBuffer() const { return gBuffer; }
        
        int getWidth()  const { return width; }
        int getHeight() const { return height; }
        
        Shader& getScreenShader() { return screenShader; }
    protected:
        GLuint gBuffer;
        GLuint gPosition, gNormal, gAlbedo;
        Shader screenShader;
        Shader linearShader;
        Shader quaterShader;
        // fullscreen quad
        GLuint quadVAO, quadVBO;
    private:
        int width, height;
};
