#include "Scene.h"
#include "effect/SSAO.h"
#include "effect/FXAA.h"
#include "effect/Lighting.h"
#include "core/Shader.h"
#include "core/Camera.h"
#include "core/Window.h"

#include <iostream>

int main()
{
    const GLuint SCR_WIDTH = 1280, SCR_HEIGHT = 720;
    
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    Window window(SCR_WIDTH, SCR_HEIGHT);
    Camera &camera = window.getCamera();
    window.hideCursor();
    
    camera.Position = {3.7007,2.70767,6.96745};
    camera.Yaw = -152;
    camera.Pitch = -27.5;
    camera.updateCameraVectors();

    GBuffer gbuffer(SCR_WIDTH, SCR_HEIGHT);
    SSAO ssao(&gbuffer);
    FXAA fxaa(&gbuffer);
    Lighting lighting(&gbuffer);
    
    Scene scene(&window);

    glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

    while(window.run())
    {
        glm::mat4 view = camera.GetViewMatrix();
        glm::mat4 projection = camera.GetProjectionMatrix(window.getAspect());
        
        // render the scene into multi-render-to-texture
        gbuffer.bind(view, projection);
        glPolygonMode(GL_FRONT_AND_BACK, window.wireframe ? GL_LINE : GL_FILL);
        scene.render(&gbuffer);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        gbuffer.unbind();

        // post-processing
        ssao.render(view, projection);
        fxaa.render(view, projection);
        
        lighting.render(
            view, projection,
            //gbuffer->getFBAlbedo(),
            fxaa.getFBFXAA(),
            ssao.getFBBlured(),
            window.draw_mode
        );
        
        window.swap();
    }
    
    glfwTerminate();
    return 0;
}