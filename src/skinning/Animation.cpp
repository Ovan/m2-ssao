#include "Animation.h"

#include <iostream>

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Animation::Animation(std::string path)
    : Model(), m_num_bones(0), ticks_per_second(0)
{
    scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
    
    if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }
    
    std::cout << "Loaded : " << path << std::endl;
    
		aiMatrix4x4 tp1 = scene->mRootNode->mTransformation;
		m_global_inverse_transform = converttoMat4(tp1);
    m_global_inverse_transform = glm::inverse(m_global_inverse_transform);
    
    directory = path.substr(0, path.find_last_of('/'));
    processNode(scene->mRootNode);
    
    std::cout << "\tscene->HasAnimations() : " << scene->HasAnimations() << std::endl;
    std::cout << "\tscene->mNumMeshes : " << scene->mNumMeshes << std::endl;
    std::cout << "\tscene->mNumAnimations : " << scene->mNumAnimations << std::endl;
    
    if(scene->HasAnimations())
    {
        std::cout << "\tscene->mAnimations[0]->mName : " << scene->mAnimations[0]->mName.C_Str() << std::endl;
        std::cout << "\tscene->mAnimations[0]->mNumChannels : " << scene->mAnimations[0]->mNumChannels << std::endl;
        std::cout << "\tscene->mAnimations[0]->mDuration : " << scene->mAnimations[0]->mDuration << std::endl;
        std::cout << "\tscene->mAnimations[0]->mTicksPerSecond : " << scene->mAnimations[0]->mTicksPerSecond << std::endl;
        std::cout << "\tscene->mAnimations[0]->mDuration : " << scene->mAnimations[0]->mDuration << std::endl;
        ticks_per_second = scene->mAnimations[0]->mTicksPerSecond;
    }
}

void Animation::processNode(aiNode* node)
{
    Model::processNode(node);
    
    for(GLuint i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        auto buffer = meshes[i];
        processAnim(buffer, mesh);
    }
}

void Animation::processAnim(std::shared_ptr<Mesh> buffer, aiMesh* mesh)
{
    for (unsigned int i = 0; i < mesh->mNumBones; i++)
    {
        unsigned int bone_index = 0;
        std::string bone_name(mesh->mBones[i]->mName.data);

        if(m_bone_mapping.find(bone_name) == m_bone_mapping.end())
        {
            bone_index = m_num_bones;
            m_num_bones++;
            BoneMatrix bi;
            m_bone_matrices.push_back(bi);
            m_bone_matrices[bone_index].offset = converttoMat4(mesh->mBones[i]->mOffsetMatrix);
            m_bone_mapping[bone_name] = bone_index;
        }
        else
        {
            bone_index = m_bone_mapping[bone_name];
        }

        for (unsigned int j = 0; j<mesh->mBones[i]->mNumWeights; j++)
        {
            unsigned int vertex_id = mesh->mBones[i]->mWeights[j].mVertexId;
            float weight = mesh->mBones[i]->mWeights[j].mWeight;
            buffer->vertices[vertex_id].addBoneData(bone_index, weight);
        }
    }
    
    buffer->update();
}

unsigned int Animation::findPosition(float p_animation_time, const aiNodeAnim* p_node_anim) const
{
    for(unsigned int i = 0; i < p_node_anim->mNumPositionKeys - 1; i++)
        if(p_animation_time < (float)p_node_anim->mPositionKeys[i + 1].mTime)
            return i;
    return 0;
}

unsigned int Animation::findRotation(float p_animation_time, const aiNodeAnim* p_node_anim) const
{
    for(unsigned int i = 0; i < p_node_anim->mNumRotationKeys - 1; i++)
        if(p_animation_time < (float)p_node_anim->mRotationKeys[i + 1].mTime)
            return i;
    return 0;
}

unsigned int Animation::findScaling(float p_animation_time, const aiNodeAnim* p_node_anim) const
{
    for(unsigned int i = 0; i < p_node_anim->mNumScalingKeys - 1; i++)
        if(p_animation_time < (float)p_node_anim->mScalingKeys[i + 1].mTime)
            return i;
    return 0;
}

glm::vec3 Animation::calcInterpolatedPosition(float p_animation_time, const aiNodeAnim* p_node_anim)
{
    if(p_node_anim->mNumPositionKeys == 1)
        return glm::make_vec3(&p_node_anim->mPositionKeys[0].mValue.x);

    unsigned int position_index = findPosition(p_animation_time, p_node_anim);
    unsigned int next_position_index = position_index + 1;
    
    float delta_time = (float)(p_node_anim->mPositionKeys[next_position_index].mTime - p_node_anim->mPositionKeys[position_index].mTime);
    float factor = (p_animation_time - (float)p_node_anim->mPositionKeys[position_index].mTime) / delta_time;
    
    aiVector3D start = p_node_anim->mPositionKeys[position_index].mValue;
    aiVector3D end = p_node_anim->mPositionKeys[next_position_index].mValue;
    aiVector3D delta = end - start;
    aiVector3D interpolated = start + factor * delta;
    
    return glm::make_vec3(&interpolated.x);
}

glm::fquat Animation::calcInterpolatedRotation(float p_animation_time, const aiNodeAnim* p_node_anim)
{
    if(p_node_anim->mNumRotationKeys == 1)
        return glm::make_quat(&p_node_anim->mRotationKeys[0].mValue.x);

    unsigned int rotation_index = findRotation(p_animation_time, p_node_anim);
    unsigned int next_rotation_index = rotation_index + 1;
    
    float delta_time = (float)(p_node_anim->mRotationKeys[next_rotation_index].mTime - p_node_anim->mRotationKeys[rotation_index].mTime);
    float factor = (p_animation_time - (float)p_node_anim->mRotationKeys[rotation_index].mTime) / delta_time;
    
    aiQuaternion start_quat = p_node_anim->mRotationKeys[rotation_index].mValue;
    aiQuaternion end_quat = p_node_anim->mRotationKeys[next_rotation_index].mValue;

    return nlerp(start_quat, end_quat, factor);
}

glm::vec3 Animation::calcInterpolatedScaling(float p_animation_time, const aiNodeAnim* p_node_anim)
{
    if(p_node_anim->mNumScalingKeys == 1)
        return glm::make_vec3(&p_node_anim->mScalingKeys[0].mValue.x);

    unsigned int scaling_index = findScaling(p_animation_time, p_node_anim);
    unsigned int next_scaling_index = scaling_index + 1;
    
    float delta_time = (float)(p_node_anim->mScalingKeys[next_scaling_index].mTime - p_node_anim->mScalingKeys[scaling_index].mTime);
    float  factor = (p_animation_time - (float)p_node_anim->mScalingKeys[scaling_index].mTime) / delta_time;
    
    aiVector3D start = p_node_anim->mScalingKeys[scaling_index].mValue;
    aiVector3D end = p_node_anim->mScalingKeys[next_scaling_index].mValue;
    aiVector3D delta = end - start;
    aiVector3D interpolated = start + factor * delta;
    
    return glm::make_vec3(&interpolated.x);
}

const aiNodeAnim * Animation::findNodeAnim(const aiAnimation * p_animation, const std::string p_node_name) const
{
    for(unsigned int i = 0; i < p_animation->mNumChannels; i++)
    {
        const aiNodeAnim* node_anim = p_animation->mChannels[i];
        if(std::string(node_anim->mNodeName.data) == p_node_name)
            return node_anim;
    }

    return nullptr;
}