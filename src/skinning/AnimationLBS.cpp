#include "AnimationLBS.h"

#include <iostream>

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

AnimationLBS::AnimationLBS(std::string path)
    : Animation(path)
{
}

void AnimationLBS::readNodeHierarchy(float p_animation_time, const aiNode* p_node, const glm::mat4 parent_transform)
{
    std::string node_name(p_node->mName.data);

    const aiAnimation* animation = scene->mAnimations[0];
    const aiNodeAnim* node_anim = findNodeAnim(animation, node_name);
    glm::mat4 node_transform = converttoMat4(p_node->mTransformation);

    if(node_anim)
    {
        glm::mat4 translate = glm::translate(glm::mat4(1.0), calcInterpolatedPosition(p_animation_time, node_anim));
        glm::mat4 rotate = glm::mat4_cast(calcInterpolatedRotation(p_animation_time, node_anim));
        glm::mat4 scale = glm::scale(glm::mat4(1.0), calcInterpolatedScaling(p_animation_time, node_anim));
        node_transform = translate * rotate * scale;
    }

    glm::mat4 global_transform = parent_transform * node_transform;

    if(m_bone_mapping.find(node_name) != m_bone_mapping.end())
    {
        unsigned int bone_index = m_bone_mapping[node_name];
        m_bone_matrices[bone_index].final_world_transform = m_global_inverse_transform * global_transform * m_bone_matrices[bone_index].offset;
    }

    for(unsigned int i = 0; i < p_node->mNumChildren; i++)
        readNodeHierarchy(p_animation_time, p_node->mChildren[i], global_transform);
}

std::vector<glm::mat4> AnimationLBS::boneTransform(float time_in_sec)
{
    double time_in_ticks = time_in_sec * ticks_per_second;
    float animation_time = fmod(time_in_ticks, scene->mAnimations[0]->mDuration);
    
    readNodeHierarchy(animation_time, scene->mRootNode, glm::mat4());
    
    std::vector<glm::mat4> transforms(m_num_bones);

    for(unsigned int i = 0; i < m_num_bones; i++)
        transforms[i] = m_bone_matrices[i].final_world_transform;
        
    return transforms;
}

void AnimationLBS::draw(const Shader &shader, float time)
{
    shader.bind();
    
    glUniformMatrix4fv(
        glGetUniformLocation(shader.Program, "model"),
        1, GL_FALSE, glm::value_ptr(model)
    );
    
    if(scene->HasAnimations())
    {
        std::vector<glm::mat4> transforms = boneTransform(time);
        glm::mat4 identity = glm::mat4(1.0);
        
        for(unsigned int i = 0; i < transforms.size(); ++i)
        {
            const std::string name = "anim_bones[" + std::to_string(i) + "]";
            GLuint boneTransform = glGetUniformLocation(shader.Program, name.c_str());
            glUniformMatrix4fv(boneTransform, 1, GL_FALSE, glm::value_ptr(transforms[i]));
        }
        
        for(unsigned int i = transforms.size(); i<100; i++)
        {
            const std::string name = "anim_bones[" + std::to_string(i) + "]";
            GLuint boneTransform = glGetUniformLocation(shader.Program, name.c_str());
            glUniformMatrix4fv(boneTransform, 1, GL_FALSE, glm::value_ptr(identity));
        }
    }
    
    for(GLuint i = 0; i < meshes.size(); i++)
        meshes[i]->draw(shader);
}