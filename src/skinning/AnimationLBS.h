#pragma once

#include "Animation.h"

class AnimationLBS : public Animation
{
    public:
        AnimationLBS(std::string path);
        
        virtual void draw(const Shader &shader, float time=0.f);
    protected:
        void readNodeHierarchy(float p_animation_time, const aiNode* p_node, const glm::mat4 parent_transform);
        std::vector<glm::mat4> boneTransform(float time_in_ses);
};
