#pragma once

#include "Animation.h"

class AnimationDQS : public Animation
{
    public:
        AnimationDQS(std::string path);
        
        virtual void draw(const Shader &shader, float time=0.f);
    protected:
        void readNodeHierarchy(float AnimationTime, const aiNode* pNode, const glm::fdualquat& ParentTransform);
        std::vector<glm::fdualquat> boneTransform(float TimeInSeconds);
    private:
};
