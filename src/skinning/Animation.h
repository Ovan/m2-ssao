#pragma once

#include "../core/Model.h"
#include "Math.h"

struct BoneMatrix
{
    glm::mat4 offset;
    glm::mat4 final_world_transform;
    glm::fdualquat final_world_transform_dq;
};

class Animation : public Model
{
    public:
        Animation(std::string path);
    protected:
        virtual void processNode(aiNode* node);
        void processAnim(std::shared_ptr<Mesh> buffer, aiMesh* mesh);
        // animation
        const aiNodeAnim* findNodeAnim(const aiAnimation * p_animation, const std::string p_node_name) const;
        // keys position
        unsigned int findPosition(float p_animation_time, const aiNodeAnim* p_node_anim) const;
        unsigned int findRotation(float p_animation_time, const aiNodeAnim* p_node_anim) const;
        unsigned int findScaling(float p_animation_time, const aiNodeAnim* p_node_anim) const;
        // interpolation between keys
        glm::vec3 calcInterpolatedPosition(float p_animation_time, const aiNodeAnim* p_node_anim);
        glm::fquat calcInterpolatedRotation(float p_animation_time, const aiNodeAnim* p_node_anim);
        glm::vec3 calcInterpolatedScaling(float p_animation_time, const aiNodeAnim* p_node_anim);
    protected:
        std::vector<BoneMatrix> m_bone_matrices;
        std::map<std::string, unsigned int> m_bone_mapping;
        glm::mat4 m_global_inverse_transform;
        unsigned int m_num_bones;
        float ticks_per_second;
};
