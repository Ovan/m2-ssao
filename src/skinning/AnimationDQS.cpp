#include "AnimationDQS.h"

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

AnimationDQS::AnimationDQS(std::string path)
    : Animation(path)
{
}

void AnimationDQS::readNodeHierarchy(float p_animation_time, const aiNode* p_node, const glm::fdualquat& parent_transform)
{
		std::string node_name(p_node->mName.data);
    
    const aiAnimation* animation = scene->mAnimations[0];
    const aiNodeAnim* node_anim = findNodeAnim(animation, node_name);
    
		glm::fdualquat node_transformation_dq; // = glm::dualquat_cast(converttoMat4(p_node->mTransformation));
    
		if(node_anim)
    {
        auto rotationQ = calcInterpolatedRotation(p_animation_time, node_anim);
        auto Translation = calcInterpolatedPosition(p_animation_time, node_anim);
        node_transformation_dq = glm::fdualquat(rotationQ, Translation);
		}
    
		glm::fdualquat global_transformation_dq = parent_transform * node_transformation_dq;

		if(m_bone_mapping.find(node_name) != m_bone_mapping.end())
    {
        unsigned int bone_index = m_bone_mapping[node_name];

        glm::fdualquat offset_dq = dualquat_cast(m_bone_matrices[bone_index].offset);
        glm::fdualquat inverse_transform_dq = dualquat_cast(m_global_inverse_transform);
        
        glm::fdualquat offset_dq_norm = glm::normalize(offset_dq);
        glm::fdualquat inverse_transform_dq_norm = glm::normalize(inverse_transform_dq);
        glm::fdualquat global_transformation_dq_norm = glm::normalize(global_transformation_dq);
        
        m_bone_matrices[bone_index].final_world_transform_dq = inverse_transform_dq_norm * global_transformation_dq_norm * offset_dq_norm;
		}

		for(unsigned int i = 0; i < p_node->mNumChildren; i++)
        readNodeHierarchy(p_animation_time, p_node->mChildren[i], global_transformation_dq);
}

std::vector<glm::fdualquat> AnimationDQS::boneTransform(float time_in_sec)
{
    double time_in_ticks = time_in_sec * ticks_per_second;
    float animation_time = fmod(time_in_ticks, scene->mAnimations[0]->mDuration);
    
    readNodeHierarchy(animation_time, scene->mRootNode, glm::fdualquat());
    
    std::vector<glm::fdualquat> transforms(m_num_bones);

    for(unsigned int i = 0; i < m_num_bones; i++)
        transforms[i] = glm::normalize(m_bone_matrices[i].final_world_transform_dq);
        
    return transforms;
}

void AnimationDQS::draw(const Shader &shader, float time)
{
    shader.bind();
    
    glUniformMatrix4fv(
        glGetUniformLocation(shader.Program, "model"),
        1, GL_FALSE, glm::value_ptr(model)
    );
    
    if(scene->HasAnimations())
    {
        std::vector<glm::fdualquat> transforms = boneTransform(time);
        glm::mat2x4 identity;
        
        for(unsigned int i = 0; i < transforms.size(); ++i)
        {
            auto oglmat = glm::mat2x4_cast(transforms[i]);
            const std::string name = "anim_bones[" + std::to_string(i) + "]";
            
            GLuint boneTransform = glGetUniformLocation(shader.Program, name.c_str());
            glUniformMatrix2x4fv(boneTransform, 1, GL_FALSE, glm::value_ptr(oglmat));
        }
        
        for(unsigned int i = transforms.size(); i<100; i++)
        {
            const std::string name = "anim_bones[" + std::to_string(i) + "]";
            GLuint boneTransform = glGetUniformLocation(shader.Program, name.c_str());
            glUniformMatrix2x4fv(boneTransform, 1, GL_FALSE, glm::value_ptr(identity));
        }
    }
    
    for(GLuint i = 0; i < meshes.size(); i++)
        meshes[i]->draw(shader);
}